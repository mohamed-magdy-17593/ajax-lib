import { TOKEN_KEY } from "../app.const";

export const getAuthToken = () => 
  localStorage.getItem(TOKEN_KEY)

export const getAuthHeaders = () => ({
    'Authorization': `Bearer ${getAuthToken()}`
  })
