import { getReq, getAuthReq, postAuthReq } from "./ajax.lib";

export const getCountries = () => 
  getReq('/countries')
    .then(res => res.countries)

export const getLeaseContracts = () =>
  getAuthReq('/lease_contracts_list')
    .then(res => res.requests)

export const getApiProfile = () =>
  getAuthReq('/api_profile')
    .then(res => res.data.user)

export const login = (userData) =>
  postAuthReq('/api_login', userData)
    .then(res => res.token)
    
export const postConstruction = (constructionData) =>
  postAuthReq('/construction', constructionData)
    .then(res => res)

export const getChatThreads = () =>
  getAuthReq('/chat_threads')
    .then(res => res)
