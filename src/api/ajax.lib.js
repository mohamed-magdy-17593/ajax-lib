import { urlFor } from "../utils/functions.utils"
import { getAuthHeaders } from "./auth"
import isEmpty from 'ramda/src/isEmpty'

function requestFormatter(method, urlPath, data) {
  if (method.toUpperCase() === 'GET') {
    return [
      isEmpty(data)
        ? urlPath
        : `${urlPath}?${new URLSearchParams(data).toString()}`,
      null,
      {}
    ]
  }
  if (data instanceof FormData) { 
    return [
      urlPath,
      data,
      {},
    ]
  }
  return [
    urlPath,
    JSON.stringify(data),
    { 'Content-Type': 'application/json; charset=utf-8'},
  ]
}

/**
 * HOC for create requests methods
 * @param {string} method 
 * @param {object} defoultHeaders 
 */
export const createRequest = (method='get', defoultHeaders={}) =>
  (urlPath='', data={}, custumHeaders={}) => {
    const [ url, body, headers ] = requestFormatter(method, urlPath, data)
    return fetch(urlFor(url), {
      method: method.toUpperCase(),
      headers: {
        ...headers,
        ...defoultHeaders,
        ...custumHeaders
      },
      body,
    })
    .then(res => res.json())
    .catch(err => console.error(urlPath, err) || err)
  }

// all normal methods

export const getReq = createRequest('GET')

export const postReq = createRequest('POST')

export const putReq = createRequest('PUT')

export const deleteReq = createRequest('DELETE')

// all auth methods

export const getAuthReq = createRequest('GET', getAuthHeaders())

export const postAuthReq = createRequest('POST', getAuthHeaders())

export const putAuthReq = createRequest('PUT', getAuthHeaders())

export const deleteAuthReq = createRequest('DELETE', getAuthHeaders())
