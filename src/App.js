import React from 'react'
import { getLeaseContracts } from './api/ajax.lib.test';

class App extends React.Component {

  componentWillMount() {
    getLeaseContracts()
      .then(res => console.log(res))
  }
  
  render() {
    return (
      <div className="helvetica">
        <h1 className="tc fw1">Ajax lib</h1>
      </div>
    )
  }

}

export default App
