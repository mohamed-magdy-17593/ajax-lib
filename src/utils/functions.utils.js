import { API_URL } from "../app.const"

/**
 * create url based on urlpath
 * @param {string} url 
 */
export const urlFor = (url) => 
  /^http/.test(url)
    ? url
    : url.startsWith('/')
      ? `${API_URL}${url}`
      : `${API_URL}/${url}`
